---
title: Monoculturas do pensamento e a importância do reflorestamento do imaginário
slug: monoculturas-do-pensamento
source: RevistaCom, ano 8, no. 21, 2021
aliases:
- /zine/monoculturas-do-pensamento
authors: Geni Núñez
---

O ensaio discute a relação que há entre a teoria e a prática da violência colonial, problematizando os efeitos do sistema de monocultura na conjuntura dos negacionismos. A partir da cosmogonia anticolonial guarani, o texto busca apresentar pistas para o reflorestamento das relações do humano entre si e com os demais seres.