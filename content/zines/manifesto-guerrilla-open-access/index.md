---
title: Manifesto da Guerrilla Open Access
slug: manifesto-guerrilla-open-access
aliases:
- /zine/manifesto-guerrilla-open-access
authors: Aaron Swartz
---

Informação é poder. Mas, como todo o poder, há aqueles que querem mantê-lo para si mesmos. A herança inteira do mundo científico e cultural, publicada ao longo dos séculos em livros e revistas, é cada vez mais digitalizada e trancada por um punhado de corporações privadas.