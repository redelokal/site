#!/bin/bash

if [[ -z "$MESSAGE" ]]; then
    echo "\$MESSAGE variable not defined"
    exit 1
fi

rm -rf public/

hugo

git add .
git commit -m "$MESSAGE"
git push -u origin main

rsync -a --delete --filter 'P .git' --filter 'P .domains' public/ ../pages/

pushd ../pages

if [[ $(git status --porcelain) ]]; then
    git add .
    git commit -m "$MESSAGE"
    git push -u origin main
fi

popd