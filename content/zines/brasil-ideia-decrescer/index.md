---
title: O Que Pode Vir a Ser No Brasil A Ideia de Decrescer?
slug: brasil-ideia-decrescer
source: 'Decrescimento: Vocabulário para um novo mundo. Porto Alegre: Tomo Editorial, 2016'
aliases:
- /zine/brasil-ideia-decrescer
authors: Felipe Milanez
---

A publicação desse texto em formato de zine é um convite à leitura do livro Decrescimento: Um vocabulário para um novo mundo. 

&nbsp;  

O livro contém verbetes escritos por diversas pessoas do mundo afora, construindo um glossário de terminologias, teorias e práticas que visam explorar alternativas ao crescimento desenfreado promovido pelo sistema capitalista.

&nbsp;  

Livro disponível em português [aqui](https://drive.google.com/file/d/19r9pvFgAueR6uqQEv_PymlvPrLcAKuPq/view).