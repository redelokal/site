---
title: Foda-se o Neoliberalismo
slug: foda-se-neoliberalismo
source: https://acme-journal.org/index.php/acme/article/view/1342/1188
authors: Simon Springer
---

Cansado somente do trabalho acadêmico e intelectual, Simon Springer oferece um artigo de revolta direta contra o neoliberalismo, nos convidando a produzir e prefigurar um futuro diferente – tudo isso enquanto levantamos o dedo do meio e gritamos FODA-SE O NEOLIBERALISMO.