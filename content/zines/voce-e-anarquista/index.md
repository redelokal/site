---
title: Você é Anarquista?
slug: voce-e-anarquista
source: https://bibliotecaanarquista.org/library/david-graeber-voce-e-anarquista
aliases:
- /zine/voce-e-anarquista
authors: David Graeber
---

O fato é que nada disso está perto de ser verdade. Anarquistas são simplesmente pessoas que acreditam que seres humanos são capazes de se comportar de maneira razoável sem ter de ser forçados a isso. É realmente uma noção bem simples, mas é algo que os ricos e poderosos sempre consideraram extremamente perigoso.