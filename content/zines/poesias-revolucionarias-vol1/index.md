---
title: Poesias Revolucionárias Vol. 1
slug: poesias-revolucionarias-vol-1
source: '"Poesias de Luta da América Latina" por Jeff Vasques, Recanto das Letras e TruncaPoemas'
aliases:
- /zine/poesias-revolucionarias-vol-1
---

Poesias de luta.

Poesias de resistência.

Poesias revolucionárias.

&nbsp;

No primeiro volume, as seguintes poesias:
- A única mulher que pode ser – Bertalicia Peralta
- Entre os muros fechados – Carmen Soler
- Perguntas de um operário que lê – Bertolt Brecht
- Gritaram-me negra – Victoria Santa Cruz