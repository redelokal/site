---
title: Finanças
---

Custos e ganhos da Rede L*o*kal.

&nbsp;

**Caixa: R$52**.

&nbsp;

---

&nbsp;

Agosto / 2024:
- Zines - 6 - R$24

&nbsp;

Março / 2024:
- Zines - 7 - R$28

&nbsp;

Fevereiro / 2024:
- Impressora: -R$1534
- Grampeador: -R$49
- Grampos: -R$5
- Refiladora: -R$137
