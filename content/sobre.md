---
title: Sobre
---

Distribuindo conhecimento que liberta.

&nbsp;

Instagram: [@redelokal](https://www.instagram.com/redelokal/).  
Grupo do WhatsApp: [Rede Lokal](https://chat.whatsapp.com/KvzALudTIwx381je84f12v).  
Finanças: Veja todas os custos e ganhos [aqui](/financas).
